package com.photoclique.photocliquerestapi.service;

import com.photoclique.photocliquerestapi.model.Photographer;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class PhotographerService {

    public List<Photographer> getPhotographers(){
        return Collections.emptyList();
    }

    public Photographer getPhotographer(String photographerId) {
        return new Photographer();
    }
}
