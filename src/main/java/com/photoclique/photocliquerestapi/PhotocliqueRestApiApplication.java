package com.photoclique.photocliquerestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PhotocliqueRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PhotocliqueRestApiApplication.class, args);
	}

}
