package com.photoclique.photocliquerestapi.controller;

import com.photoclique.photocliquerestapi.model.Photographer;
import com.photoclique.photocliquerestapi.service.PhotographerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;


@RestController
@RequestMapping("/api/v1/photographers")
public class PhotographerController {

    @Resource
    private PhotographerService photographerService;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Photographer> getPhotograpers(){
        return photographerService.getPhotographers();
    }

    @GetMapping(value = "/{photographerId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Photographer getPhotograper(@PathVariable ("photographerId") String photographerId){
        return photographerService.getPhotographer(photographerId);
    }


}
